package com.example.DCloset.Model.SendBack;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class SendBackCreateRequest {

    private LocalDate sendBackDate;
    private String sendBackNumber;
}
