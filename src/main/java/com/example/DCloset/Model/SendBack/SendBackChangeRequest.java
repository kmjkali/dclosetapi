package com.example.DCloset.Model.SendBack;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class SendBackChangeRequest {



    private LocalDate sendBackDate;
    private String sendBackNumber;

}
