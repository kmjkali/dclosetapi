package com.example.DCloset.Model.Delivery;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DeliveryChangeRequest {


    private LocalDate deliveryDate;
    private String deliveryNumber;



}
