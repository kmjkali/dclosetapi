package com.example.DCloset.Model.Product;

import com.example.DCloset.Enums.ProductSize;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ProductResponse {

    private Long id;
    private LocalDate productCreateDate;
    private String productName;
    private Integer productCode;
    private String productInfo;
    private String productSize;
    private String productColor;
    private Boolean nyMembership;
    private Boolean nyOffline;
    private Boolean nyFree;
    private Boolean nyOneDay;
    private Boolean nyPost;
    private Integer useAbleQuantity;
    private Integer usingQuantity;
    private Integer repairQuantity;

    private String productMainImage;
    private String productSubImage1;
    private String productSubImage2;
    private String productSubImage3;





}
