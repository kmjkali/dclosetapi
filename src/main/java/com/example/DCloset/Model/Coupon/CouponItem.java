package com.example.DCloset.Model.Coupon;

import com.example.DCloset.Enums.CouponType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class CouponItem {

    private long id;
    private String couponName;
    private LocalDate couponCreateDate;
    private LocalDate couponEndDate;
    private CouponType couponType;
    private String couponExplain;
}
