package com.example.DCloset.Model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberDupCheckResponse {

    private Boolean isNew;
}

