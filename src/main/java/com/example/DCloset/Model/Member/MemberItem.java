package com.example.DCloset.Model.Member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String username;
    private String memberName;
    private String memberPw;
    private LocalDate birthDay;
    private String phoneNumber;
    private String memberAddress;
    private String memberDetailedAddress;
    private Integer postCode;
    private LocalDate subscriptDate;
    private Boolean nyPersonalInfo;
    private Boolean nyMarketing;
    private String memberGrade;
    private String payWay;
    private String payInfo;
    private String payDay;
}
