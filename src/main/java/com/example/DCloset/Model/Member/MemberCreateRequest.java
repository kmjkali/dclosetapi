package com.example.DCloset.Model.Member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberCreateRequest {
    private String username;
    private String memberPw;
    private String MemberPwRe;
    private String memberName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String memberAddress;
    private String memberDetailedAddress;
    private Integer postCode;
    private Boolean nyPersonalInfo;
    private Boolean nyMarketing;
}
