package com.example.DCloset.Model.Charge;

import com.example.DCloset.entity.Member;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@ToString
public class ChargeCreateRequest {

    private Long memberId;
    private LocalDate actualPayDay;
    private Integer payAmount;
    private Integer discountAmount;
    private Integer finalAmount;
    private Integer valueOfVat;
    private Boolean nyCashReceipt;
    private Boolean nyTaxInvoice;




}
