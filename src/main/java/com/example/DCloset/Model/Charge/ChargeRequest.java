package com.example.DCloset.Model.Charge;

import com.example.DCloset.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ChargeRequest {

    private Member member;
    private LocalDate actualPayDay;
    private Integer payAmount;
    private Integer discountAmount;
    private Integer finalAmount;
    private Integer valueOfVat;
    private Boolean nyCashReceipt;
    private Boolean nyTaxInvoice;

}
