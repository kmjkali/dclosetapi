package com.example.DCloset.Service;


import com.example.DCloset.Model.Coupon.CouponChangeRequest;
import com.example.DCloset.Model.Coupon.CouponItem;
import com.example.DCloset.Model.Coupon.CouponCreateRequest;
import com.example.DCloset.Model.Coupon.CouponResponse;
import com.example.DCloset.Repository.CouponRepository;
import com.example.DCloset.entity.Coupon;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class CouponService {

    private final CouponRepository couponRepository;

    public void setCoupon(CouponCreateRequest request) {

        Coupon addData = new Coupon();

        addData.setCouponName(request.getCouponName());
        addData.setCouponCreateDate(request.getCouponCreateDate());
        addData.setCouponEndDate(request.getCouponEndDate());
        addData.setCouponExplain(request.getCouponExplain());
        addData.setCouponType(request.getCouponType());

        couponRepository.save(addData);


    }

    public List<CouponItem> getcouponList(){

        List<Coupon> originList = couponRepository.findAll();

        List<CouponItem> result = new LinkedList<>();

        for(Coupon coupon:originList){
            CouponItem addList = new CouponItem();

            addList.setId(coupon.getId());
            addList.setCouponName(coupon.getCouponName());
            addList.setCouponCreateDate(coupon.getCouponCreateDate());
            addList.setCouponEndDate(coupon.getCouponEndDate());
            addList.setCouponExplain(coupon.getCouponExplain());
            addList.setCouponType(coupon.getCouponType());

            result.add(addList);
        }

        return result;
    }

    public CouponResponse getCoupon(long id){

        Coupon originData = couponRepository.findById(id).orElseThrow();

        CouponResponse response = new CouponResponse();

        response.setId(originData.getId());
        response.setCouponName(originData.getCouponName());
        response.setCouponCreateDate(originData.getCouponCreateDate());
        response.setCouponEndDate(originData.getCouponEndDate());
        response.setCouponExplain(originData.getCouponExplain());
        response.setCouponType(originData.getCouponType());

        return response;
    }

    public void putCouponChange (long id, CouponChangeRequest request){

        Coupon originData = couponRepository.findById(id).orElseThrow();

        originData.setCouponName(request.getCouponName());
        originData.setCouponCreateDate(request.getCouponCreateDate());
        originData.setCouponEndDate(request.getCouponEndDate());
        originData.setCouponExplain(request.getCouponExplain());
        originData.setCouponType(request.getCouponType());

        couponRepository.save(originData);


    }

    public void delCoupon(long id) {
        couponRepository.deleteById(id);
    }

}

