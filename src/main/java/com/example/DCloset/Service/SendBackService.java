package com.example.DCloset.Service;

import com.example.DCloset.Model.SendBack.SendBackCreateRequest;
import com.example.DCloset.Model.SendBack.SendBackItem;
import com.example.DCloset.Model.SendBack.SendBackResponse;
import com.example.DCloset.Repository.SendBackRepository;
import com.example.DCloset.entity.SendBack;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SendBackService {

    private final SendBackRepository sendBackRepository;


    public void setReturn(SendBackCreateRequest request){

        SendBack addDate = new SendBack();

        addDate.setSendBackDate(request.getSendBackDate());
        addDate.setSendBackNumber(request.getSendBackNumber());

        sendBackRepository.save(addDate);
    }

    public List<SendBackItem> getSendBackList(){

        List<SendBack> originList = sendBackRepository.findAll();
        List<SendBackItem> result = new LinkedList<>();

        for(SendBack sendBack:originList) {
            SendBackItem addList = new SendBackItem();
            addList.setSendBackDate(sendBack.getSendBackDate());
            addList.setSendBackNumber(sendBack.getSendBackNumber());

            result.add(addList);
        }

        return result;

    }

    public SendBackResponse getSendBack(long id) {

        SendBack originData =  sendBackRepository.findById(id).orElseThrow();

        SendBackResponse response = new SendBackResponse();

        response.setId(originData.getId());
        response.setSendBackDate(originData.getSendBackDate());
        response.setSendBackNumber(originData.getSendBackNumber());

        return response;


    }

    public void putSendBackChange(long id, SendBackCreateRequest request) {

        SendBack originData = sendBackRepository.findById(id).orElseThrow();

        originData.setSendBackNumber(request.getSendBackNumber());
        originData.setSendBackDate(request.getSendBackDate());

        sendBackRepository.save(originData);


    }

    public void delSendBack(long id) {
        sendBackRepository.deleteById(id);
    }






}
