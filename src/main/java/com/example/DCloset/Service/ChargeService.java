package com.example.DCloset.Service;


import com.example.DCloset.Model.Charge.ChargeRequest;
import com.example.DCloset.Model.Charge.ChargeCreateRequest;
import com.example.DCloset.Model.Charge.ChargeInfoItem;
import com.example.DCloset.Model.Charge.ChargeResponse;
import com.example.DCloset.Repository.ChargeRepository;
import com.example.DCloset.Repository.MemberRepository;
import com.example.DCloset.entity.Charge;
import com.example.DCloset.entity.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChargeService {

    private final ChargeRepository chargeRepository;
    private final MemberRepository memberRepository;

    public void setCharge(ChargeCreateRequest request){

        Charge addData = new Charge();

        Member member = memberRepository.findById(request.getMemberId()).orElseThrow();

        addData.setMember(member);
        addData.setActualPayDay(request.getActualPayDay());
        addData.setPayAmount(request.getPayAmount());
        addData.setDiscountAmount(request.getDiscountAmount());
        addData.setFinalAmount(request.getFinalAmount());
        addData.setValueOfVat(request.getValueOfVat());
        addData.setNyCashReceipt(request.getNyCashReceipt());
        addData.setNyTaxInvoice(request.getNyTaxInvoice());

        chargeRepository.save(addData);
    }


    /**
     *
     * @return
     */
    public List<ChargeInfoItem> getChargeList(){

        List<Charge> originList = chargeRepository.findAll();
        List<ChargeInfoItem> result = new LinkedList<>();

        for(Charge payInfo:originList) {
            ChargeInfoItem addList = new ChargeInfoItem();

            addList.setId(payInfo.getId());
            addList.setActualPayDay(payInfo.getActualPayDay());
            addList.setPayAmount(payInfo.getPayAmount());
            addList.setDiscountAmount(payInfo.getDiscountAmount());
            addList.setFinalAmount(payInfo.getFinalAmount());
            addList.setValueOfVat(payInfo.getValueOfVat());
            addList.setNyCashReceipt(payInfo.getNyCashReceipt());
            addList.setNyTaxInvoice(payInfo.getNyTaxInvoice());

            result.add(addList);

        }
        return result;
    }

    public ChargeResponse getCharge(long id) {

        Charge originData = chargeRepository.findById(id).orElseThrow();

        ChargeResponse response = new ChargeResponse();

        response.setId(originData.getId());
        response.setActualPayDay(originData.getActualPayDay());
        response.setPayAmount(originData.getPayAmount());
        response.setDiscountAmount(originData.getDiscountAmount());
        response.setFinalAmount(originData.getFinalAmount());
        response.setValueOfVat(originData.getValueOfVat());
        response.setNyCashReceipt(originData.getNyCashReceipt());
        response.setNyTaxInvoice(originData.getNyTaxInvoice());
        return response;

    }

    public void putChargeChangeRequest(long id, ChargeRequest request){

       Charge originData = chargeRepository.findById(id).orElseThrow();

       originData.setMember(request.getMember());
       originData.setActualPayDay(request.getActualPayDay());
       originData.setPayAmount(request.getPayAmount());
       originData.setDiscountAmount(request.getDiscountAmount());
       originData.setFinalAmount(request.getFinalAmount());
       originData.setValueOfVat(request.getValueOfVat());
       originData.setNyCashReceipt(request.getNyCashReceipt());
       originData.setNyTaxInvoice(request.getNyTaxInvoice());
       chargeRepository.save(originData);
        
    }





}
