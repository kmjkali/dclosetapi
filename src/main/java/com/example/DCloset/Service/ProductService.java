package com.example.DCloset.Service;

import com.example.DCloset.Enums.ProductSize;
import com.example.DCloset.Model.Product.ProductCreateRequest;
import com.example.DCloset.Model.Product.ProductItem;
import com.example.DCloset.Model.Product.ProductResponse;
import com.example.DCloset.Repository.ProductRepository;
import com.example.DCloset.entity.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor

public class ProductService {

    private final ProductRepository productRepository;


    public void setProduct(ProductCreateRequest request){

        Product addData = new Product();
        addData.setProductCreateDate(request.getProductCreateDate());
        addData.setProductName(request.getProductName());
        addData.setProductCode(request.getProductCode());
        addData.setProductInfo(request.getProductInfo());
        addData.setProductSize(request.getProductSize());
        addData.setProductColor(request.getProductColor());
        addData.setNyMembership(request.getNyMembership());
        addData.setNyOffline(request.getNyOffline());
        addData.setNyFree(request.getNyFree());
        addData.setNyOneDay(request.getNyOneDay());
        addData.setNyPost(request.getNyPost());
        addData.setUseAbleQuantity(request.getUseAbleQuantity());
        addData.setUsingQuantity(request.getUsingQuantity());
        addData.setRepairQuantity(request.getRepairQuantity());

        addData.setProductMainImage(request.getProductMainImage());
        addData.setProductSubImage1(request.getProductSubImage1());
        addData.setProductSubImage2(request.getProductSubImage2());
        addData.setProductSubImage3(request.getProductSubImage3());





        log.debug("Parameter Data => {}", addData);
        productRepository.save(addData);

    }

    public List<ProductItem> getList(){

        List<Product> originList = productRepository.findAll();

        List<ProductItem> result = new LinkedList<>();

        for (Product product:originList) {
            ProductItem addList = new ProductItem();

            addList.setId(product.getId());
            addList.setProductCreateDate(product.getProductCreateDate());
            addList.setProductName(product.getProductName());
            addList.setProductCode(product.getProductCode());
            addList.setProductInfo(product.getProductInfo());
            addList.setProductSize(product.getProductSize().name());
            addList.setProductColor(product.getProductColor());
            addList.setNyMembership(product.getNyMembership());
            addList.setNyOffline(product.getNyOffline());
            addList.setNyPost(product.getNyPost());
            addList.setNyOneDay(product.getNyOneDay());
            addList.setNyFree(product.getNyFree());
            addList.setUseAbleQuantity(product.getUseAbleQuantity());
            addList.setUsingQuantity(product.getUsingQuantity());
            addList.setRepairQuantity(product.getRepairQuantity());

            addList.setProductMainImage(product.getProductMainImage());
            addList.setProductSubImage1(product.getProductSubImage1());
            addList.setProductSubImage2(product.getProductSubImage2());
            addList.setProductSubImage3(product.getProductSubImage3());


            result.add(addList);





        }
        return result;


    }

    public ProductResponse getProduct(long id) {

        Product originData = productRepository.findById(id).orElseThrow();

        ProductResponse response = new ProductResponse();

        response.setId(originData.getId());
        response.setProductCreateDate(originData.getProductCreateDate());
        response.setProductName(originData.getProductName());
        response.setProductCode(originData.getProductCode());
        response.setProductInfo(originData.getProductInfo());
        response.setProductSize(originData.getProductSize().name());
        response.setProductColor(originData.getProductColor());
        response.setNyMembership(originData.getNyMembership());
        response.setNyOffline(originData.getNyOffline());
        response.setNyFree(originData.getNyFree());
        response.setNyPost(originData.getNyPost());
        response.setUseAbleQuantity(originData.getUseAbleQuantity());
        response.setUsingQuantity(originData.getUsingQuantity());
        response.setRepairQuantity(originData.getRepairQuantity());

        response.setProductMainImage(originData.getProductMainImage());
        response.setProductSubImage1(originData.getProductSubImage1());
        response.setProductSubImage2(originData.getProductSubImage2());
        response.setProductSubImage3(originData.getProductSubImage3());



        return response;


    }


    public void putProductChange (long id, ProductCreateRequest request){

        Product originData = productRepository.findById(id).orElseThrow();

        originData.setProductCreateDate(request.getProductCreateDate());
        originData.setProductName(request.getProductName());
        originData.setProductSize(request.getProductSize());
        originData.setProductCode(request.getProductCode());
        originData.setProductInfo(request.getProductInfo());
        originData.setProductColor(request.getProductColor());
        originData.setNyMembership(request.getNyMembership());
        originData.setNyOffline(request.getNyOffline());
        originData.setNyOneDay(request.getNyOneDay());
        originData.setNyFree(request.getNyFree());
        originData.setNyPost(request.getNyPost());
        originData.setUseAbleQuantity(request.getUseAbleQuantity());
        originData.setUsingQuantity(request.getUsingQuantity());
        originData.setRepairQuantity(request.getRepairQuantity());

        originData.setProductMainImage(request.getProductMainImage());
        originData.setProductSubImage1(request.getProductSubImage1());
        originData.setProductSubImage2(request.getProductSubImage2());
        originData.setProductSubImage3(request.getProductSubImage3());

        productRepository.save(originData);


    }


    public void delProduct(long id) {
        productRepository.deleteById(id);

    }


}
