package com.example.DCloset.Service;

import com.example.DCloset.Model.Delivery.DeliveryCreateRequest;
import com.example.DCloset.Model.Delivery.DeliveryItem;
import com.example.DCloset.Model.Delivery.DeliveryResponse;
import com.example.DCloset.Repository.DeliveryRepository;
import com.example.DCloset.entity.Delivery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final DeliveryRepository deliveryRepository;

    public void setDelivery(DeliveryCreateRequest request) {

        Delivery addDate = new Delivery();

        addDate.setDeliveryDate(request.getDeliveryDate());
        addDate.setDeliveryNumber(request.getDeliveryNumber());

        deliveryRepository.save(addDate);

    }

    public List<DeliveryItem> getDeliveryList() {

        List<Delivery> originList = deliveryRepository.findAll();
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery:originList) {
            DeliveryItem addList = new DeliveryItem();
            addList.setDeliveryDate(delivery.getDeliveryDate());
            addList.setDeliveryNumber(delivery.getDeliveryNumber());

            result.add(addList);
        }

        return result;


    }


    public DeliveryResponse getDelivery(long id){

        Delivery originData = deliveryRepository.findById(id).orElseThrow();

        DeliveryResponse response = new DeliveryResponse();

        response.setId(originData.getId());
        response.setDeliveryDate(originData.getDeliveryDate());
        response.setDeliveryNumber(originData.getDeliveryNumber());

        return response;




    }

    public void  putDeliveryChange(long id, DeliveryCreateRequest request) {

        Delivery originData = deliveryRepository.findById(id).orElseThrow();

        originData.setDeliveryDate(request.getDeliveryDate());
        originData.setDeliveryNumber(request.getDeliveryNumber());

        deliveryRepository.save(originData);
    }






    public void delDelivery(long id) {
        deliveryRepository.deleteById(id);
    }


}