package com.example.DCloset.Service;


import com.example.DCloset.Enums.MemberGrade;
import com.example.DCloset.Enums.MemberGroup;

import com.example.DCloset.Enums.PayDay;
import com.example.DCloset.Enums.PayWay;
import com.example.DCloset.Model.Delivery.DeliveryResponse;
import com.example.DCloset.Model.Member.MemberCreateRequest;
import com.example.DCloset.Model.Member.MemberDupCheckResponse;
import com.example.DCloset.Model.Member.MemberItem;
import com.example.DCloset.Model.Member.MemberJoinRequest;
import com.example.DCloset.Repository.MemberRepository;
import com.example.DCloset.entity.Delivery;
import com.example.DCloset.entity.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public MemberDupCheckResponse getMemberIdDupCheck(String username) {
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUsername(username));
        return response;
    }


    public Member getMemberInfo(long id) {
        // 회원의 정보값을 id만 받아와야 하는지 다른 Join 테이블을 만든 후에 확인해보기
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setUsername(request.getUsername());
        addData.setMemberPw(request.getMemberPw());
        addData.setMemberName(request.getMemberName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMemberAddress(request.getMemberAddress());
        addData.setMemberDetailedAddress(request.getMemberDetailedAddress());
        addData.setPostCode(request.getPostCode());
        addData.setSubscriptDate(LocalDate.now());
        addData.setNyMarketing(request.getNyMarketing());
        addData.setNyPersonalInfo(request.getNyPersonalInfo());
        // 등급과 권한을 회원가입시 기본값 일반회원으로 고정.
        addData.setMemberGrade(MemberGrade.NORMAL);
        addData.setMemberGroup(MemberGroup.NORMAL);
        addData.setPayWay(PayWay.NONE);
        addData.setPayInfo(null);
        addData.setPayDay(PayDay.NONE);

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new ArrayList<>();

        for(Member member : originList ) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setUsername(member.getUsername());
            addItem.setMemberName(member.getMemberName());
            addItem.setBirthDay(member.getBirthDay());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setMemberAddress(member.getMemberAddress());
            addItem.setMemberDetailedAddress(member.getMemberDetailedAddress());
            addItem.setPostCode(member.getPostCode());
            addItem.setSubscriptDate(member.getSubscriptDate());
            addItem.setNyPersonalInfo(member.getNyPersonalInfo());
            addItem.setNyMarketing(member.getNyMarketing());
            addItem.setMemberGrade(member.getMemberGrade().getMemberGrade());
            addItem.setPayWay(member.getPayWay().getPayWay());
            addItem.setPayInfo(member.getPayInfo());
            addItem.setPayDay(member.getPayDay().getPayDay());

            result.add(addItem);
        }

        return result;
    }


    /**
     * 회원을 가입시칸다.
     *
     * @param request 회원가입장에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    public void  setMemberJoin(MemberJoinRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (!request.getMemberPw().equals(request.getMemberPwRe())) throw new Exception();

    }


    /**
     *  신규 아이디 인지 확인한다
     *  *
     * @param username 아이디
     * @return true 신규아이디/ false 중복아이디
     */

    private boolean isNewUsername(String username) {

        long dupCount = memberRepository.countByUsername(username);

        return dupCount > 0;



    }








}
