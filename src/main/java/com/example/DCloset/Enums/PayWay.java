package com.example.DCloset.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayWay {
    BANK_ACCOUNT("계좌 이체"),
    CREDIT_CARD("카드 이체"),
    NONE("결제정보 없음");

    private final String payWay;
}
