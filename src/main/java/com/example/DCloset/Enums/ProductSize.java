package com.example.DCloset.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductSize {

    SMALL("S"),
    MEDIUM("M"),
    LARGE("L");

    private final String productSize;


}
