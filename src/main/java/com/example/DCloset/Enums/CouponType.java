package com.example.DCloset.Enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor


public enum CouponType {

    EVENT("이벤트 쿠폰"),
    LONG_TERM("장기구독혜택");

    private final String couponType;

}
