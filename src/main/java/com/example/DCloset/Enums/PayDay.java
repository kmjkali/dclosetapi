package com.example.DCloset.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayDay {
    FIFTH("5일"),
    TENTH("10일"),
    FIFTEEN("15일"),
    TWENTY("20일"),
    TWENTY_FIVE("25일"),
    NONE("결제정보 없음");

    private final String payDay;
}
