package com.example.DCloset.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    NORMAL("일반회원"),
    ADMIN("관리자");

    private final String memberGroup;
}
