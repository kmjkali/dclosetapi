package com.example.DCloset.Controller;

import com.example.DCloset.Model.Member.MemberCreateRequest;
import com.example.DCloset.Model.Member.MemberDupCheckResponse;
import com.example.DCloset.Model.Member.MemberJoinRequest;
import com.example.DCloset.Service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);
        return "OK";
    }

    @PostMapping("/join")
    public String setMemberJoin(@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMemberJoin(request);
        return "ok";

    }

    //@PathVariable-필수 Paramiter- 선택
    //타입이 String이니까 paramiter
    //param도 username 이름을 맞춰준다

    @GetMapping("/check/id")
    public MemberDupCheckResponse getMemberIdCheck(
            @RequestParam(name = "username")String username,
            @RequestParam(name = "goodsName",required = false,defaultValue = "몰라" ) String goodsName,
            @RequestParam(name = "minPrice",required = false,defaultValue = "0")double minPrice

            ){

         return memberService.getMemberIdDupCheck(username);


    }

}
