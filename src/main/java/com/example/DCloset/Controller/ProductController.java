package com.example.DCloset.Controller;


import com.example.DCloset.Model.Product.ProductCreateRequest;
import com.example.DCloset.Model.Product.ProductItem;
import com.example.DCloset.Model.Product.ProductResponse;
import com.example.DCloset.Service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product")

public class ProductController {

    private final ProductService productService;

    @PostMapping("/new")
    public String setProduct(@RequestBody ProductCreateRequest request){

        productService.setProduct(request);
        return "ok";
    }

    @GetMapping("/all")
    public List<ProductItem> getProductList(){

        return productService.getList();
    }

    @GetMapping("/detail/{id}")
    public ProductResponse getProduct(@PathVariable long id) {

        return productService.getProduct(id);
    }

    @PutMapping("/product/{id}")
    public ProductCreateRequest putProductChange(@PathVariable long id,@RequestBody ProductCreateRequest request){

        productService.putProductChange(id, request);

        return putProductChange(id, request);
    }

    @DeleteMapping("/delete/{id}")
    public String delProduct(@PathVariable long id) {

        productService.delProduct(id);

        return "OK";
    }
}
