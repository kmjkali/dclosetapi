package com.example.DCloset.Controller;


import com.example.DCloset.Model.Coupon.CouponChangeRequest;
import com.example.DCloset.Model.Coupon.CouponItem;
import com.example.DCloset.Model.Coupon.CouponCreateRequest;
import com.example.DCloset.Model.Coupon.CouponResponse;
import com.example.DCloset.Service.CouponService;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/coupon")
public class CouponController {

    private final CouponService  couponService;

    @PostMapping("/new")
    public String setCoupon(@RequestBody CouponCreateRequest request){

        couponService.setCoupon(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<CouponItem> getCouponList(){
        return couponService.getcouponList();
    }

    @GetMapping("/detail/{id}")
    public CouponResponse getCoupon(@PathVariable long id) {

        return couponService.getCoupon(id);

    }
    @PutMapping("/coupon/{id}")
    public String putCouponChange(@PathVariable long id, @RequestBody CouponChangeRequest request){

        couponService.putCouponChange(id,request);

        return "ok";

    }

    @DeleteMapping("/delete/{id}")
    public String delCoupon(@PathVariable long id){
        couponService.delCoupon(id);

        return "ok";

    }

}
