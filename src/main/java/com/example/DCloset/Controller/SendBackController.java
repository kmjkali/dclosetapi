package com.example.DCloset.Controller;

import com.example.DCloset.Model.SendBack.SendBackChangeRequest;
import com.example.DCloset.Model.SendBack.SendBackCreateRequest;
import com.example.DCloset.Model.SendBack.SendBackItem;
import com.example.DCloset.Model.SendBack.SendBackResponse;
import com.example.DCloset.Service.SendBackService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/turn")
public class SendBackController {

    private final SendBackService sendBackService;

    @PostMapping("/new")
    public String setReturn(@RequestBody SendBackCreateRequest request) {

        sendBackService.setReturn(request);
        return "ok";

    }

    @GetMapping("/all")
    public List<SendBackItem> getSendBackList() {

        return sendBackService.getSendBackList();
    }

    @GetMapping("/detail/{id}")
    public SendBackResponse getSendBack(@PathVariable long id) {

        return sendBackService.getSendBack(id);

    }

    @PutMapping("/send_back/{id}")
    public String putSendBackChange(@PathVariable long id,@RequestBody SendBackCreateRequest request){

        sendBackService.putSendBackChange(id,request);

        return "ok";

    }

    @DeleteMapping("/delete/{id}")

    public String delSendBack(@PathVariable long id){

        sendBackService.delSendBack(id);

        return "ok";


    }


}
