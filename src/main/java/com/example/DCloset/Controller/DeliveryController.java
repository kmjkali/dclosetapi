package com.example.DCloset.Controller;

import com.example.DCloset.Model.Delivery.DeliveryCreateRequest;
import com.example.DCloset.Model.Delivery.DeliveryItem;
import com.example.DCloset.Model.Delivery.DeliveryResponse;
import com.example.DCloset.Service.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/DLVY")
public class DeliveryController {

     private final DeliveryService deliveryService;

     @PostMapping("/new")
    public String setDelivery(@RequestBody DeliveryCreateRequest request){

         deliveryService.setDelivery(request);

         return "ok";
     }
     @GetMapping("/all")
    public List<DeliveryItem> getDeliveryList() {
         return deliveryService.getDeliveryList();
     }

     @GetMapping("/detail/{id}")
    public DeliveryResponse getDelivery (@PathVariable long id) {
         return deliveryService.getDelivery(id);
     }

     @PutMapping("/delivery/{id}")
    public String putDeliveryChange(@PathVariable long id, @RequestBody DeliveryCreateRequest request){

         deliveryService.putDeliveryChange(id,request);

         return "ok";
     }





}
