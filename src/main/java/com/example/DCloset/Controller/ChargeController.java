package com.example.DCloset.Controller;


import com.example.DCloset.Model.Charge.ChargeCreateRequest;
import com.example.DCloset.Model.Charge.ChargeInfoItem;
import com.example.DCloset.Model.Charge.ChargeRequest;
import com.example.DCloset.Model.Charge.ChargeResponse;
import com.example.DCloset.Service.ChargeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/charge")
public class ChargeController {

    private final ChargeService chargeService;

    @PostMapping("/new")
    public String setCharge(@RequestBody ChargeCreateRequest request){

        chargeService.setCharge(request);
        return "ok";

    }
    @GetMapping("/all")
    public List<ChargeInfoItem> getChargeList(){
       return chargeService.getChargeList();

    }
    @GetMapping("/detail/{id}")
    public ChargeResponse getCharge(@PathVariable long id){
        return chargeService.getCharge(id);

    }
    @PutMapping("pay_info/{id}")
    public String putChargeChangeRequest(@PathVariable long id, ChargeRequest request){


        chargeService.putChargeChangeRequest(id, request);
        return "ok";


    }


}
