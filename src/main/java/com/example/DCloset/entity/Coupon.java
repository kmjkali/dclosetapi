package com.example.DCloset.entity;

import com.example.DCloset.Enums.CouponType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Coupon {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false,length = 20)
    private String couponName;

    @Column(nullable = false)
    private LocalDate couponCreateDate;

    @Column(nullable = false)
    private LocalDate couponEndDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private CouponType couponType;

    @Column(nullable = false,columnDefinition = "TEXT")
    private String couponExplain;

}
