package com.example.DCloset.entity;


import com.example.DCloset.Enums.ProductSize;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Entity
@Setter
@Getter
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate productCreateDate;

    @Column(nullable = false,length = 20)
    private String productName;

    @Column(nullable = false)
    private Integer productCode;

    @Column(nullable = false,columnDefinition = "text")
    private String productInfo;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private ProductSize productSize;

    @Column(nullable = false, length = 10)
    private String productColor;

    @Column(nullable = true)
    private String productMainImage;
    @Column(nullable = true)
    private String productSubImage1;
    @Column(nullable = true)
    private String productSubImage2;
    @Column(nullable = true)
    private String productSubImage3;

    @Column(nullable = false)
    private Boolean nyMembership;

    @Column(nullable = false)
    private Boolean nyOffline;

    @Column(nullable = false)
    private Boolean nyFree;

    @Column(nullable = false)
    private Boolean nyOneDay;

    @Column(nullable = false)
    private Boolean nyPost;

    @Column(nullable = false)
    private Integer useAbleQuantity;

    @Column(nullable = false)
    private Integer usingQuantity;

    @Column(nullable = false)
    private Integer repairQuantity;

}
