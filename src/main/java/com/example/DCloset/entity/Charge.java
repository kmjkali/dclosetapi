package com.example.DCloset.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity

public class Charge {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;


    @Column(nullable = false)
    private LocalDate actualPayDay;

    @Column(nullable = false)
    private Integer payAmount;

    @Column(nullable = false)
    private Integer discountAmount;

    @Column(nullable = false)
    private Integer finalAmount;

    @Column(nullable = false)
    private Integer valueOfVat;

    @Column(nullable = false)
    private Boolean nyCashReceipt;

    @Column(nullable = false)
    private Boolean nyTaxInvoice;
}
