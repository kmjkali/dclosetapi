package com.example.DCloset;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DClosetApplication {

	public static void main(String[] args) {
		SpringApplication.run(DClosetApplication.class, args);
	}

}
