package com.example.DCloset.Repository;

import com.example.DCloset.entity.Member;

import org.springframework.data.jpa.repository.JpaRepository;



public interface MemberRepository extends JpaRepository<Member, Long> {
//


    long countByUsername(String username);

    //유저네임에 일치하는 데이터의 숫자를 세고 싶어
    //long을 쓰는 이유는 아이디 수만큼 PK수만큼 가져오기위해서



}
