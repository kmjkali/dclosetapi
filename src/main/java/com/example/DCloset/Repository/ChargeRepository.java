package com.example.DCloset.Repository;

import com.example.DCloset.entity.Charge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChargeRepository extends JpaRepository<Charge,Long> {
}
