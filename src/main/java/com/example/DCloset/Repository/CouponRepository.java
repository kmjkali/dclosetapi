package com.example.DCloset.Repository;


import com.example.DCloset.entity.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Coupon,Long > {
}
