package com.example.DCloset.Repository;

import com.example.DCloset.entity.SendBack;


import org.springframework.data.jpa.repository.JpaRepository;



public interface SendBackRepository extends JpaRepository<SendBack,Long> {
}
